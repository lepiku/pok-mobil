Permainan Mobil
===============
Tugas Akhir POK

Angota Kelompok 12:
* Muhammad Oktoluqman Fakhrianto (1806186723)
* Nasywa Nur Fathiyah (1806205546)
* Timothy Regana Tarigan (1806205041)

## Deskripsi
Permainan Mobil adalah permainan dimana pemain harus menggerakan mobil untuk
menghindari jalan yang penuh hambatan. Mobil pemain dilambangkan dengan `'@'`
dan hambatan di jalan dilambangkan dengan `'#'`. Pemain dapat memindahkan
mobilnya ke dua jalur yang disediakan dengan menekan sebuah tombol. Pemain
harus meraih skor setinggi-tingginya. Video demo aplikasi jalannya program
dapat dilihat di [https://youtu.be/TPeqJdVuU7o](https://youtu.be/TPeqJdVuU7o).

## Tugas
- [X] Setup LCD dan LED
- [X] Buat main menu
- [X] Taro player di kiri
- [X] Buat algo pseudorandom untuk random posisi hambatan
- [X] Meletakkan hambatan di paling kanan
- [X] Buat variabel 2 posisi hambatan terakhir (dimasukkan ke var position)
- [X] Ada 2 hambatan yang ditampilkan
- [X] While loop yang terus menggeser hambatan ke kiri
- [X] Setup interrupt, saat player pencet tombol, posisi player pindah
- [X] Kalau pemain lewatin hambatan, skor ditambah 1
- [X] Menampilkan skor di LED dalam binary
- [X] Perlahan-lahan permainan bertambah cepat
- [X] kalau pemain nabrak, permainan berhenti
- [X] Setelah nabrak, menampilkan skor pemain
- [X] Tekan tombol untuk kembali ke permainan
- [X] Tampilkan skor tertinggi di game over

## Pemakaian
1. Buka AVRStudio dan HAPSIM.exe
2. Dalam AVRStudio, buat projek baru, gunakan ATMega8515
3. Copy-Paste file program (mobil.asm) ke projek yang baru dibuat
4. Assemble and run (Ctrl+F7) projek anda, lalu run program (F5)
5. Dalam HAPSIM, buka konfigurasi permainan, dan pastikan AVRStudio Hook
   sudah dicentang di bagian options
6. Untuk memulai permainan, pencet tombol Start di HAPSIM
7. Untuk menggerakkan pemain, pencet tombol move untuk menghindari rintangan
8. Bila menabrak rintangan (game over), pencet tombol start untuk memulai
   kembali permainan
