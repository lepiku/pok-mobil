; CONFIGURATION
; PORTA = LCD pins
; PORTB = LCD data
; PORTC = LED

; BUTTON:
; Start = Start game
; Move = Lompat/Pindah tempat


; DEFINITIONS
.include "m8515def.inc"
.def dummy = r15 ; one-time use register to initialize data memory
.def temp = r16 ; temporary register
.def temp2 = r17 ; temp for delay and storing highscore to memory
.def temp3 = r25
.def state = r18	;0b00 = menu
					;0b01 / 0b11 = game
					;bit 1: move flag

.def position = r19	;bit 0: player position
					;bit 1: further obstacle position
					;bit 2: closer obstacle position
.def cache_random = r20 ; used for random algorithm
.def score = r21
.def obs_pos = r22
.def temp_move = r23
.def delaycounter = r24


; RESET and INTERRUPT VECTORS
.org $00
	rjmp MAIN
.org $01
	rjmp ext_int0
.org $02
	rjmp ext_int1


; CODE SEGMENT
MAIN:
	clr dummy
	STS $0060, dummy ; initalize data memory to zero
init_stack:
	ldi temp, low(RAMEND)
	ldi temp, high(RAMEND)
	out SPH, temp

init_output:
	ser temp ; load $FF to temp
	out DDRA,temp ; Set port A as output
	ser temp ; load $FF to temp
	out DDRB,temp ; Set port B as output
	ser temp ; load $FF to temp
	out DDRC,temp ; Set port C to output

init_interrupt:
	ldi temp, (1<<ISC2)
	out EMCUCR, temp
	ldi temp, 0b00001010
	out MCUCR,temp
	ldi temp,0b11100000
	out GICR,temp
	sei

init_lcd:
	cbi PORTA,1 ; CLR RS
	ldi temp,$38 ; --> 8bit, 2line, 5x7
	out PORTB,temp
	sbi PORTA,0 ; SET EN
	cbi PORTA,0

	cbi PORTA,1 ; CLR RS
	ldi temp,0x0C ; --> disp ON, cursor OFF, blink OFF
	out PORTB,temp
	sbi PORTA,0 ; SET EN
	cbi PORTA,0

	rcall CLEAR_LCD

	cbi PORTA,1 ; CLR RS
	ldi temp,$06 ; --> increase cursor, display scroll OFF
	out PORTB,temp
	sbi PORTA,0 ; SET EN
	cbi PORTA,0

init_led:
	ldi temp,0x01 ; reset LED
	out PORTC,temp
	ldi temp,0x00
	out PORTC,temp

init_random:
	ldi cache_random, $ED ; random seed

;================ MENU ============================================
MENU:
	ldi ZH,high(2 * menu_message) ; Load high part of byte address into ZH
	ldi ZL,low(2 * menu_message) ; Load low part of byte address into ZL
	rcall loadbyte ; write menu_message

	; pindah cursor ke bawah
	cbi PORTA,1 ; CLR RS
	ldi temp_move, 0b11000000
	out PORTB, temp_move
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN

	ldi ZH,high(2 * menu_message2) ; Load high part of byte address into ZH
	ldi ZL,low(2 * menu_message2) ; Load low part of byte address into ZL
	rcall loadbyte ; write menu_message

	ldi state, 0

menu_loop:
	cpi state, 1 ; If int0 is pressed, play the game
	breq PLAY

	rjmp menu_loop

;================ PLAY ============================================
PLAY: ; initialize program to start playing
	ldi position, 0
	ldi obs_pos, 8
	ldi delaycounter, 2
	ldi score, 0
	rcall CLEAR_LCD
	rcall draw_player
	rcall dec_cursor
	rjmp random

play_loop:
	; draw the closer obstacle
	ldi temp, 0b10000000
	add temp, obs_pos
	sbrc position, 2
	subi temp, -64

	cbi PORTA, 1
	out PORTB, temp ; move cursor to obstacle
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN

	rcall draw_space
	rcall draw_obstacle

	; draw the further obstacle
	ldi temp, 0b10001000
	add temp, obs_pos
	sbrc position, 1
	subi temp, -64

	cbi PORTA, 1
	out PORTB, temp ; move cursor to obstacle
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN

	rcall draw_space
	rcall draw_obstacle

	rcall DELAY_01
	dec obs_pos

	cpi obs_pos, 1
	breq compare_pos
back:
	cpi obs_pos, 0 ; put new obstacle on random position
	breq goto_random
	rjmp play_loop
goto_random:
	rjmp random

draw_player: ; draw position of the player
	ldi ZH,high(2 * player_char) ; Load high part of byte address into ZH
	ldi ZL,low(2 * player_char) ; Load low part of byte address into ZL
	rcall loadbyte ; write player
	ret

draw_obstacle:
	ldi ZH,high(2 * obstacle_char) ; Load high part of byte address into ZH
	ldi ZL,low(2 * obstacle_char) ; Load low part of byte address into ZL
	rcall loadbyte ; write player
	ret

draw_space:
	sbi PORTA, 1
	ldi temp_move, 0x20
	out PORTB, temp_move
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	ret

move_player: ; function move player
	cbr state, 2 ; set move flag to 0
	sbrc position, 0
	rjmp move_top

	; kebawah
	cbi PORTA,1 ; CLR RS
	ldi temp_move, 0b10000000 ; pindah cursor ke atas
	out PORTB, temp_move
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall draw_space

	cbi PORTA,1 ; CLR RS
	ldi temp_move, 0b11000000 ; pindah cursor ke bawah
	out PORTB, temp_move
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall draw_player

	sbr position, 1
	ret

move_top:
	;keatas
	cbi PORTA,1 ; CLR RS
	ldi temp_move, 0b11000000 ; pindah cursor ke bawah
	out PORTB, temp_move
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall draw_space

	cbi PORTA,1 ; CLR RS
	ldi temp_move, 0b10000000 ; pindah cursor ke atas
	out PORTB, temp_move
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall draw_player

	cbr position, 1
	ret

compare_pos:
	sbrc position, 0 ;skip bit if bit 0 is zero -> skip to comparing_zero
	rjmp comparing_one
	rjmp comparing_zero ;skipped to this if bit0 is zero	
comparing_zero: ;check if 00
	sbrc position, 2
	rjmp back
	rjmp GAME_OVER
comparing_one: ; check if 11
	sbrs position, 2
	rjmp back
	rjmp GAME_OVER

;================ GAME OVER =======================================
GAME_OVER:
	subi score, 1
	lds temp2, $0060 ; loads highscore in memory
	cp temp2, score 
	brlt hiscore_change ; if highscore is lower than score, change highscore

no_highscore:

	rcall CLEAR_LCD
	ldi ZH,high(2 * gameover_message) ; Load high part of byte address into ZH
	ldi ZL,low(2 * gameover_message) ; Load low part of byte address into ZL
	rcall loadbyte ; write menu_message

	cbi PORTA,1 ; CLR RS
	ldi temp_move, 0b11000000
	out PORTB, temp_move
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN

	ldi ZH,high(2 * score_message)
	ldi ZL,low(2 * score_message)
	rcall loadbyte
	ldi state, 0
	ldi temp3, 0

	rcall dec_cursor
	cbi PORTA,1 ; CLR RS
	ldi temp_move, 0b11001111
	out PORTB, temp_move
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	
	; cetak score
	rcall print_score
	; cetak high score
	mov score, temp2
	cbi PORTA,1 ; CLR RS
	ldi temp_move, 0b10001111
	out PORTB, temp_move
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall print_score

	rjmp game_over_loop

; function to print score and high score
print_score:
	cpi score, 10
	brlt print_exit

	subi temp3, -1
	subi score, 10

	rjmp print_score

print_exit: 
	sbi PORTA, 1 ; Set RS
	ldi temp, 0b00110000
	add temp, score
	out PORTB,temp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN

	mov score, temp3
	ldi temp3, 0

	cpi score, 0
	brne print_score
	ret

game_over_loop: ;displays game over screen until game is started again
	sbrc state, 0
	rjmp PLAY
	rjmp game_over_loop

hiscore_change: ;change highscore value in data memory
	mov temp2, score
	sts $0060, temp2
	rjmp no_highscore

random: ; randomize obstacle position with Linear congruential generator
	out PORTC, score
	subi score, -1
	subi delaycounter, -1
	ldi obs_pos, 8 ; reset position counter

	; delete dodged obstacle
	cbi PORTA,1 ; CLR RS
	ldi temp_move, 0b10000000
	sbrc position, 2
	subi temp_move, -64
	out PORTB, temp_move
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall draw_space

	lsl position ; shift left
	sbrc position, 1
	sbr position, 1

	; (cache_random * 123 + 234)
	ldi temp, 123
	mul cache_random, temp
	mov cache_random, r0
	subi cache_random, -234

	; check the last bit
	sbrc cache_random, 7
	rjmp obs_bottom

obs_top:
	cbr position, 2 ; put new obstacle on top
	rjmp play_loop

obs_bottom:
	sbr position, 2 ; put new obstacle on bottom
	rjmp play_loop


;================ FUNCTIONS =======================================
ext_int0:
	sbrs state, 0
	ldi state, 1
	reti

ext_int1:
	sbr state, 2 ; set move flag to 1
	reti

loadbyte:
	lpm ; Load byte from program memory into r0

	tst r0 ; check if we've reached the end of the message
	breq exit_loadbyte ; If so, exit

	; write letter
	sbi PORTA,1 ; SET RS
	out PORTB, r0 ; put the character onto port B
	sbi PORTA,0 ; SET EN
	cbi PORTA,0

	adiw ZL,1 ; Increase Z registers
	rjmp loadbyte

exit_loadbyte:
	ret

CLEAR_LCD:
	cbi PORTA,1 ; CLR RS
	ldi temp,$01 ; --> clear LCD
	out PORTB,temp
	sbi PORTA,0 ; SET EN
	cbi PORTA,0
	ret

dec_cursor:
	cbi PORTA,1 ; CLR RS
	ldi temp, 0b00000100 ; decrease cursor
	out PORTB, temp
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	ret


DELAY_01:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; DELAY_CONTROL 40 000 cycles
	; 5ms at 8.0 MHz

		ldi temp, 50 ;ori 52
		ldi temp2, 242 ;ori 242
		sub temp, delaycounter
		cpi temp, 15
		brlt TEMP_LOAD
	
		
	L1: 
		sbrc state, 1
		rcall move_player ; move if move flag is 1
		dec temp2

		brne L1
		dec temp

		brne L1
	 	nop
	ret

TEMP_LOAD:
	ldi temp, 15
	subi delaycounter, 1
	ret

menu_message:
	.db "Tugas Akhir POK", 0
menu_message2:
	.db "Permainan Mobil", 0
player_char:
	.db "@", 0
obstacle_char:
	.db "#", 0
gameover_message:
	.db "GAME | HI:", 0, 0
score_message:
	.db "OVER | SCORE:", 0
